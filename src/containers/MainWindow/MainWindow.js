import React, {Component, Fragment} from "react";
import CountryList from "../CountryList/CountryList";
import axios from "axios";
import InfoWindow from "../InfoWindow/InfoWindow";
class MainWindow extends Component {
    state = {
        allCountry: [],
        displayCountry: null
    };

    getInfo = name => {
        const index = this.state.allCountry.findIndex(p => p.name === name);

        this.setState({displayCountry: this.state.allCountry[index]})

    };

    componentDidMount() {
        console.log("didMount");
        const BASE_URL = 'https://restcountries.eu/rest/v2/';
        const POSTS_URL = 'all';
        axios.get(BASE_URL + POSTS_URL).then(response => {
            const allCountry = response.data.map(country => {
                return {...country, name: country.name}
            });
            Promise.all(allCountry).then(result => {
                this.setState({allCountry: result});


            });


        }).catch(error => {
            console.log(error);
        });
    }


    render() {
        console.log(this.state.displayCountry,'njj');
        return (
            <Fragment>
                <div className="List">{

                    this.state.allCountry.map(one => {
                        return <CountryList click={() => this.getInfo(one.name)} name={one.name} key={one.name}/>
                    })}
                </div>
                <InfoWindow info={this.state.displayCountry}/>

            </Fragment>
        )

    }
}

export default MainWindow;