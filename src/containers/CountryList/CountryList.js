import React, {Component} from "react";
import CountryName from "../../components/CountryName/CountryName";
import './CountryList.css';
class CountryList extends Component {


    render() {

        return (
            <div>{
                (<CountryName name={this.props.name} click={this.props.click}></CountryName>)}</div>)
    }
}

export default CountryList;