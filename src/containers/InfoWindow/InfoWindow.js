import React, {Component} from "react";
import "./InfoWindow.css";
import axios from "axios";

class InfoWindow extends Component {


    state = {
        borders: [],
        myCountry: null
    };


    componentDidUpdate() {
        if (this.props.info) {

            if (!this.state.myCountry || (this.state.myCountry && this.state.myCountry.name !== this.props.info.name)) {
                console.log("Update", this.state.borders[0]);

                const BASE_URL = 'https://restcountries.eu/rest/v2/alpha/';
                let borders = this.props.info.borders;


                Promise.all(borders.map(one => {


                    return axios.get(BASE_URL + one).then(
                        resp => {

                            return resp.data.name;


                        }
                    )
                })).then(r => {
                    console.log(r);
                    this.setState({borders: r, myCountry: this.props.info})
                })
            }


        }
    }

    render() {
        console.log('In RENDER',this.state.borders);
        if (this.state.myCountry)
            return (<div className="InfoWindow"><h2>{this.state.myCountry.name}</h2>
                <p>{`Capital: ${this.state.myCountry.capital}`}</p>
                <img alt="" src={this.state.myCountry.flag} width="auto" height="200px"></img>
                <p>{`Population: ${this.state.myCountry.population}`}</p>
                <div> {
                    `Bordered: ${this.state.borders.join(', ')}`}</div>
            </div>);
        else  return (<div className="InfoWindow"><h6>Выберите страну</h6></div>)
    }
}
export default InfoWindow;