import React from "react";

const CountryName = props => {
    return (<p className="country" onClick={props.click}>{props.name}</p>);
};
export default CountryName;